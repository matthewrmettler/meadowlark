/******************
 * Initialization *
 ******************/

//Import from modules and libraries
var express = require('express');
var fortune = require('./lib/fortune.js');

//Initialize app
var app = express();

//Set up handlebars view engine
var handlebars = require('express3-handlebars').create({defaultLayout:'main'});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

//Set the port
app.set('port', process.env.PORT || 3000);

//Set up static middleware
app.use(express.static(__dirname + '/public'));


/******************
 * Routing Table  *
 ******************/

//Main
app.get('/', function(req, res){
	res.render('home');
});

//About
app.get('/about', function(req, res){
	res.render('about', { fortune: fortune.getFortune() } );
});

//Custom 404
app.use( function(req, res) {
	res.status(404);
	res.render('404');
});

//Custom 500
app.use( function(err, req, res, next) {
	console.error(err.stack);
	res.status(500);
	res.render('500');
});

/******************
 *     Start      *
 ******************/

//Start the server
app.listen(app.get('port'), function() {
	console.log('Express started on localhost:' + app.get('port'));
})