/*
 * fortune.js
 * Library for fortune telling for Meadowlark Travels website (created Chapter 4).
 */

/******************
 * Variables      *
 ******************/

var fortunes = [
	"Conquer your fears or they will conquer you.",
	"Rivers need springs.",
	"Do not fear what you don't know.",
	"You will have a pleasant surprise.",
	"Whenever possible, keep it simple.",
];

/******************
 *  Functions     *
 ******************/

//In order for something to be visible outside of this module, we must use 'exports'
//'fortunes' stays hidden!
 exports.getFortune = function() {
 	var idx = Math.floor(Math.random() * fortunes.length);
 	return fortuneCookies[idx];
 }